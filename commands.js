var http = require('http');
var fs = require('fs');
const rando = require('math-random');

if (!Object.entries) {
    Object.entries = function( obj ){
      var ownProps = Object.keys( obj ),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array!
      while (i--)
      {
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      }
      
      return resArray;
    };
  }

exports.admins = 
[
    "124136556578603009", //Timfa,
    "273597205141651460"  // Sky
]

exports.cmds = 
[
    {
        cmd: "attack",
        params: "Attacker, Defender",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the attacker's name and the defender's name with commas!",
                    typing: false
                });
                return;
            }

            args = args.join(" ").split(",");

            var attacker = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Attacker '" + args[0].trim() + "' not found";

            if(attacker)
            {
                var defender = bot.data[info.serverId].characters[args[1].toUpperCase().trim()];
                var form = "Defender '" + args[1].trim() + "' not found";

                if(defender)
                {
                    var attackRoll = Math.ceil(RollMod(GetToHitMod(attacker)));
                    var defendRoll = GetAC(defender);
                      
                    if(attackRoll >= defendRoll)
                    {
                        attacker.prevAttackDidFail = false;
                        var dmg = DamageRoll(attacker);
                        defender.HP = Math.max(0, defender.HP - dmg);

                        defender.CHAOS = Math.min(defender.CHAOS + Math.ceil(dmg / 2), GetMaxCHAOS(defender));

                        form = attacker.Name + " successfully attacks " + defender.Name + " for " + dmg + " points of damage! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }
                    else
                    {
                        attacker.prevAttackDidFail = true;
                        attacker.prevTarget = defender.Name;

                        attacker.CHAOS = Math.min(attacker.CHAOS + 1, GetMaxCHAOS(attacker));
                        form = attacker.Name + " fails to attack " + defender.Name + "! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "rerollattack",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            args = args.join(" ");

            var attacker = bot.data[info.serverId].characters[args.toUpperCase().trim()];
            var form = "Attacker '" + args.trim() + "' not found";

            if(attacker)
            {
                if(!attacker.prevAttackDidFail)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You can only re-roll failed attacks!",
                        typing: false
                    });
                    return;
                }

                if(attacker.CHAOS < 2)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You do not have enough CHAOS to reroll! You need at least 2 CHAOS.",
                        typing: false
                    });
                    return;
                }

                attacker.CHAOS -= 2;

                attacker.prevTarget = attacker.prevTarget || "error";

                var defender = bot.data[info.serverId].characters[attacker.prevTarget.toUpperCase().trim()];
                var form = "Defender '" + attacker.prevTarget.trim() + "' not found";

                if(defender)
                {
                    var attackRoll = Math.ceil(RollMod(GetToHitMod(attacker)));
                    var defendRoll = GetAC(defender);
                      
                    if(attackRoll >= defendRoll)
                    {
                        attacker.prevAttackDidFail = false;
                        var dmg = DamageRoll(attacker);
                        defender.HP = Math.max(0, defender.HP - dmg);

                        defender.CHAOS = Math.min(defender.CHAOS + Math.ceil(dmg / 2), GetMaxCHAOS(defender));

                        form = attacker.Name + " successfully attacks " + defender.Name + " for " + dmg + " points of damage! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }
                    else
                    {
                        attacker.prevAttackDidFail = false;
                        form = attacker.Name + " fails to attack " + defender.Name + "! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "chaosattack",
        params: "Attacker, Defender",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the attacker's name and the defender's name with commas!",
                    typing: false
                });
                return;
            }

            args = args.join(" ").split(",");

            var attacker = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Attacker '" + args[0].trim() + "' not found";

            if(attacker)
            {
                var defender = bot.data[info.serverId].characters[args[1].toUpperCase().trim()];
                var form = "Defender '" + args[1].trim() + "' not found";

                if(defender)
                {
                    var attackRoll = Math.ceil(RollMod(GetToHitMod(attacker) + attacker.CHAOS));
                    var defendRoll = GetAC(defender);

                    attacker.prevAttackDidFail = false;
                      
                    if(attackRoll >= defendRoll)
                    {
                        var dmg = DamageRoll(attacker) + Math.ceil(attacker.CHAOS / 3);
                        defender.HP = Math.max(0, defender.HP - dmg);

                        defender.CHAOS = Math.min(defender.CHAOS + Math.ceil(dmg / 2), GetMaxCHAOS(defender));
                        
                        attacker.CHAOS = 0;
                        form = attacker.Name + " successfully attacks " + defender.Name + " for " + dmg + " points of damage! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }
                    else
                    {
                        attacker.CHAOS = 0;
                        form = attacker.Name + " fails to attack " + defender.Name + "! (" + attackRoll + " vs AC" + defendRoll + ")\n" 
                        + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + ""
                    }

                    attacker.CHAOS = 0;
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "heal",
        params: "Attacker, Defender",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var healer = "";
            var healee = "";

            if(!args.join(" ").includes(","))
            {
                healer = args.join(" ");
                healee = args.join(" ");
            }
            else
            {
                args = args.join(" ").split(",");

                healer = args[0];
                healee = args[1];
            }

            var attacker = bot.data[info.serverId].characters[healer.toUpperCase().trim()];
            var form = "Healer '" + healer.trim() + "' not found";

            if(attacker)
            {
                var defender = bot.data[info.serverId].characters[healee.toUpperCase().trim()];
                var form = "Target '" + healee.trim() + "' not found";

                if(defender)
                {
                    if(attacker.CHAOS < 2)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You do not have enough CHAOS to heal! You need at least 3 CHAOS.",
                            typing: false
                        });
                        return;
                    }
    
                    attacker.CHAOS -= 3;

                    attacker.prevAttackDidFail = false;
                    var dmg = Math.floor(DamageRoll(attacker) * 0.8);
                    defender.HP = Math.min(defender.HP + dmg, GetMaxHP(defender));

                    form = attacker.Name + " heals " + (defender.Name == attacker.Name? "" : defender.Name + " ") + "for " + dmg + " points!\n" 
                    + attacker.Name + " HP: " + HealthBar(attacker.HP, GetMaxHP(attacker)) + " // CHAOS: " + ChaosBar(attacker.CHAOS, GetMaxCHAOS(attacker)) + (defender.Name == attacker.Name? "" : "\n" + defender.Name + " HP: " + " " + HealthBar(defender.HP, GetMaxHP(defender)) + " // CHAOS: " + ChaosBar(defender.CHAOS, GetMaxCHAOS(defender)) + "")
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "check",
        params: "Attacker, Stat",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name and stat name with commas!",
                    typing: false
                });
                return;
            }

            args = args.join(" ").split(",");

            var attacker = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(attacker)
            {
                args[1] = args[1].trim().toUpperCase();
                form = "Stat '" + args[1] + "' is invalid!"

                if(args[1].toUpperCase() == "ATTACK")
                    args[1] = "RANK";

                if(args[1] == "DEX" || args[1] == "CON" || args[1] == "STR" || args[1] == "INT" || args[1] == "CHA" || args[1] == "RANK")
                {
                    attackRoll = CheckStat(attacker, args[1]);

                    form = attacker.Name + " performs " + (args[1] == "INT" || args[1] == "RANK"? "an " : "a ") + args[1].replace("RANK", "attack") + (args[1] == "RANK"? ", dealing" : " Check, with a result of") + " " + Math.round(attackRoll) + (args[1] == "RANK"? " points of damage." : ".");
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "initiative",
        params: "Attacker",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            charName = args.join(" ");

            var attacker = bot.data[info.serverId].characters[charName.toUpperCase().trim()];
            var form = "Character '" + charName.trim() + "' not found";

            if(attacker)
            {
                attackRoll = Math.round(CheckDEX(attacker));

                form = attacker.Name + " has an Initiative of " + attackRoll + "!";
                attacker.CHAOS = 0;
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            });
        }
    },
    {
        cmd: "set",
        params: "Name, Stat, number",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name, the stat name and the new stat value with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                args[1] = args[1].trim().toUpperCase();

                form = "Stat name '" + args[1] + "' is invalid!";

                if(args[1] == "DEX" || args[1] == "CON" || args[1] == "STR" || args[1] == "INT" || args[1] == "CHA" || args[1] == "HP" || args[1] == "RANK" || args[1] == "CLASS" || args[1] == "CHAOS")
                {
                    if(args[1] == "RANK")
                    {
                        args[2] = args[2].trim().toUpperCase();
                        var form = "Rank '" + args[2] + "' invalid! Valid ranks are 'A', 'B' or 'C'!"

                        if(args[2] == "A" || args[2] == "B" || args[2] == "C")
                        {
                            character.RANK = args[2];

                            var form = GetCharacterDetailForm(character)
                        }
                    }
                    else if(args[1] == "CLASS")
                    {
                        args[2] = CapitalizeFirstLetter(args[2].trim().toLowerCase());

                        var form = "Class '" + args[2] + "' invalid! Valid ranks are 'A', 'B' or 'C'!"

                        if(args[2] == "Speedster" || args[2] == "Brute" || args[2] == "Martial" || args[2] == "Techie" || args[2] == "Magic" || args[2] == "Charismaniac")
                        {
                            character.CLASS = args[2];
                        
                            var form = GetCharacterDetailForm(character)
                        }
                    }
                    else
                    {
                        if (Number.isNaN(Number.parseInt(args[2]))) {
                            form = "'" + args[2].trim() + "' is not a number!";
                        }
                        else
                        {
                            if(Number.parseInt(args[2]) < (args[1] == "CHAOS"? 0:1))
                            {
                                form = "Stats cannot be lower than 1!";
                            }
                            else
                            {
                                character[args[1]] = Number.parseInt(args[2]);

                                character.HP = Math.min(character.HP, GetMaxHP(character));
                                character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                                var form = GetCharacterDetailForm(character)
                            }
                        }
                    }
                }
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "emeraldbonus",
        params: "Name, number",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name and the new emerald bonus value with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                if (Number.isNaN(Number.parseInt(args[1]))) {
                    form = "'" + args[1].trim() + "' is not a number!";
                }
                else
                {
                    if(Number.parseInt(args[1]) < 0)
                    {
                        form = "Emerald Bonus cannot be lower than 0!";
                    }
                    else
                    {
                        character.Emeralds = Number.parseInt(args[1]);

                        var form = GetCharacterDetailForm(character)
                    }
                }
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "enablesuper",
        params: "Name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            args = args.join(" ");

            var character = bot.data[info.serverId].characters[args.toUpperCase().trim()];
            var form = "Character '" + args.trim() + "' not found";

            if(character)
            {
                if(!character.SUPER)
                {
                    form = "►►" + character.Name.toUpperCase() + " IS GOING SUPER◄◄"
                    character.SUPER = true;
                    character.HP = GetMaxHP(character);
                }
                else
                {
                    form = character.Name + " is already super!";
                }
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "disablesuper",
        params: "Name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            args = args.join(" ");

            var character = bot.data[info.serverId].characters[args.toUpperCase().trim()];
            var form = "Character '" + args.trim() + "' not found";

            if(character)
            {
                form = character.Name + " is no longer super."
                character.SUPER = false;

                character.hp = Math.min(character.HP, GetMaxHP(character))
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "rename",
        params: "Name, Newname",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name and the new name with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                character.Name = args[1].trim();
                bot.data[info.serverId].characters[args[1].toUpperCase().trim()] = character;

                delete bot.data[info.serverId].characters[args[0].toUpperCase().trim()];

                var form = GetCharacterDetailForm(character)
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "add",
        params: "Name, Stat, number",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name, the stat name and the new stat value with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                args[1] = args[1].trim().toUpperCase();

                form = "Stat name '" + args[1] + "' is invalid!";

                if(args[1] == "DEX" || args[1] == "CON" || args[1] == "STR" || args[1] == "INT" || args[1] == "CHA" || args[1] == "HP" || args[1] == "CHAOS")
                {
                    
                    if (Number.isNaN(Number.parseInt(args[2]))) {
                        form = "'" + args[2].trim() + "' is not a number!";
                    }
                    else
                    {
                        if(character[args[1]]+ Number.parseInt(args[2]) < (args[1] == "CHAOS"?0:1))
                        {
                            form = "Stats cannot be lower than 1!";
                        }
                        else
                        {
                            character[args[1]] += Number.parseInt(args[2]);

                            character.HP = Math.min(character.HP, GetMaxHP(character));
                            character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                            var form = GetCharacterDetailForm(character)
                        }
                    }
                }
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "subtract",
        params: "Name, Stat, number",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(","))
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name, the stat name and the new stat value with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                args[1] = args[1].trim().toUpperCase();

                form = "Stat name '" + args[1] + "' is invalid!";

                if(args[1] == "DEX" || args[1] == "CON" || args[1] == "STR" || args[1] == "INT" || args[1] == "CHA" || args[1] == "HP" || args[1] == "CHAOS")
                {
                    
                    if (Number.isNaN(Number.parseInt(args[2]))) {
                        form = "'" + args[2].trim() + "' is not a number!";
                    }
                    else
                    {
                        if(character[args[1]] - Number.parseInt(args[2]) <  (args[1] == "CHAOS"?0:1))
                        {
                            form = "Stats cannot be lower than 1!";
                        }
                        else
                        {
                            character[args[1]] -= Number.parseInt(args[2]);

                            character.HP = Math.min(character.HP, GetMaxHP(character));
                            character.CHAOS = Math.min(character.CHAOS, GetMaxCHAOS(character));

                            var form = GetCharacterDetailForm(character)
                        }
                    }
                }
            }
            
            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "register",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "That character already exists in this server!";

            if(!character)
            {
                character = {Name:args.join(" "), HP: 1, DEX: 1, CON: 1, STR: 1, INT: 1, CHA: 1, CHAOS: 0, RANK: "C", CLASS: "None", Emeralds: 0};

                bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()] = character;
                var form = "Character Registered!\nNAME: " + character.Name;
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "delete",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "That character already exists in this server!";

            if(character)
            {
                delete bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()]

                bot.sendMessage({
                    to: info.channelID,
                    message: args.join(" ").toUpperCase().trim() + " exists no more.",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
            }
            else
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: args.join(" ").toUpperCase().trim() + " already doesn't exist.",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
            }
        }
    },
    {
        cmd: "details",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "Character not found!";

            if(character)
            {
                var form = GetCharacterDetailForm(character);
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 60000);*/
            });
        }
    },
    {
        cmd: "reset",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "Character not found!";

            if(character)
            {
                character.CLASS = "None";
                character.RANK = "C";
                character.HP = 1;
                character.DEX = 1;
                character.CON = 1;
                character.STR = 1;
                character.CHA = 1;
                character.INT = 1;
                var form = GetCharacterDetailForm(character);
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 60000);*/
            });
        }
    },
    {
        cmd: "resethealth",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "Character not found!";

            if(character)
            {
                bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()].HP = GetMaxHP(character);

                var form = character.Name
                +"\n" + HealthBar(character.HP, GetMaxHP(character))
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "health",
        params: "Character name",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            var character = bot.data[info.serverId].characters[args.join(" ").toUpperCase().trim()];
            var form = "Character not found!";

            if(character)
            {
                var form = character.Name
                +"\n" + HealthBar(character.HP, GetMaxHP(character))
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "recommended",
        params: "Character name, class name, rank",
        category: "main",
        execute: function(bot, info, args)
        {
            CheckIntegrity(bot, info);

            if(!args.join(" ").includes(",") || args.join(" ").split(",").length != 3)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command requires you to separate the character's name, the class name and the rank value with commas!",
                    typing: false
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 5000);*/
                });
                return;
            }

            args = args.join(" ").split(",");

            var character = bot.data[info.serverId].characters[args[0].toUpperCase().trim()];
            var form = "Character '" + args[0].trim() + "' not found";

            if(character)
            {
                args[2] = args[2].trim().toUpperCase();
                var form = "Rank '" + args[2] + "' invalid! Valid ranks are 'A', 'B' or 'C'!"

                if(args[2] == "A" || args[2] == "B" || args[2] == "C")
                {
                    character.RANK = args[2];

                    var form = GetCharacterDetailForm(character)
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: form,
                        typing: false
                    });
                    return;
                }
            
                args[1] = CapitalizeFirstLetter(args[1].trim().toLowerCase());

                var form = "Class '" + args[1] + "' invalid! Valid classes are 'Speedster', 'Brute', 'Martial', 'Techie', 'Magic' or 'Charismaniac'!"

                if(args[1] == "Speedster" || args[1] == "Brute" || args[1] == "Martial" || args[1] == "Techie" || args[1] == "Magic" || args[1] == "Charismaniac")
                {
                    character.CLASS = args[1];
                
                    var form = GetCharacterDetailForm(character)
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: form,
                        typing: false
                    });
                    return;
                }

                switch(character.CLASS)
                {
                    case "Speedster":
                        if(character.RANK == "A")
                        {
                            character.DEX = 18;
                            character.CON = 10;
                            character.STR = 9;
                            character.INT = 10;
                            character.CHA = 8;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 16;
                            character.CON = 9;
                            character.STR = 8;
                            character.INT = 9;
                            character.CHA = 8;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 12;
                            character.CON = 8;
                            character.STR = 7;
                            character.INT = 8;
                            character.CHA = 7;
                        }
                    break;
                    case "Brute":
                        if(character.RANK == "A")
                        {
                            character.DEX = 9;
                            character.CON = 10;
                            character.STR = 17;
                            character.INT = 8;
                            character.CHA = 11;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 9;
                            character.CON = 8;
                            character.STR = 16;
                            character.INT = 8;
                            character.CHA = 9;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 9;
                            character.CON = 7;
                            character.STR = 13;
                            character.INT = 8;
                            character.CHA = 8;
                        }
                    break;
                    case "Martial":
                        if(character.RANK == "A")
                        {
                            character.DEX = 13;
                            character.CON = 10;
                            character.STR = 15;
                            character.INT = 8;
                            character.CHA = 9;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 11;
                            character.CON = 9;
                            character.STR = 14;
                            character.INT = 8;
                            character.CHA = 8;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 10;
                            character.CON = 8;
                            character.STR = 12;
                            character.INT = 7;
                            character.CHA = 8;
                        }
                    break;
                    case "Magic":
                        if(character.RANK == "A")
                        {
                            character.DEX = 9;
                            character.CON = 10;
                            character.STR = 9;
                            character.INT = 15;
                            character.CHA = 12;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 9;
                            character.CON = 9;
                            character.STR = 8;
                            character.INT = 12;
                            character.CHA = 12;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 8;
                            character.CON = 8;
                            character.STR = 7;
                            character.INT = 10;
                            character.CHA = 12;
                        }
                    break;
                    case "Techie":
                        if(character.RANK == "A")
                        {
                            character.DEX = 9;
                            character.CON = 8;
                            character.STR = 12;
                            character.INT = 16;
                            character.CHA = 10;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 8;
                            character.CON = 8;
                            character.STR = 10;
                            character.INT = 15;
                            character.CHA = 9;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 8;
                            character.CON = 8;
                            character.STR = 9;
                            character.INT = 12;
                            character.CHA = 8;
                        }
                    break;
                    case "Charismaniac":
                        if(character.RANK == "A")
                        {
                            character.DEX = 10;
                            character.CON = 10;
                            character.STR = 9;
                            character.INT = 10;
                            character.CHA = 16;
                        }
                        if(character.RANK == "B")
                        {
                            character.DEX = 8;
                            character.CON = 8;
                            character.STR = 8;
                            character.INT = 10;
                            character.CHA = 16;
                        }
                        if(character.RANK == "C")
                        {
                            character.DEX = 8;
                            character.CON = 8;
                            character.STR = 8;
                            character.INT = 10;
                            character.CHA = 11;
                        }
                    break;
                }

                character.HP = GetMaxHP(character);
                form = GetCharacterDetailForm(character)
            }

            bot.sendMessage({
                to: info.channelID,
                message: form,
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 5000);*/
            });
        }
    },
    {
        cmd: "help",
        params: "category",
        category: "main",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Mechanics Documentation: <https://docs.google.com/presentation/d/1uSA5WpnTvZmhFCH9cukZGWc_U2mvP2Z14O3nGu_2pog/edit?usp=drivesdk>\nCommands: ```" 
                +"\n !initiative [character name]"
                +"\n !attack [attacker], [defender]"
                +"\n !chaosattack [attacker], [defender]"
                +"\n !heal [character name] [OPTIONAL: target name]"
                +"\n !check [character name], [stat name]"
                +"\n ** !characters"
                +"\n ** !details [character name]"
                +"\n * !register [character name]"
                +"\n * !reset [character name]"
                +"\n * !delete [character name]"
                +"\n * !set [character name], [stat], [value]"
                +"\n * !emeraldbonus [character name], [value]"
                +"\n * !add [character name], [stat], [value]"
                +"\n * !subtract [character name], [stat], [value]"
                +"\n * !resethealth [character name]"
                +"\n * !health [character name] ```",
                //+"\n Note: Commands with an * will only show their output for a few seconds. Commands with two ** will show their output for one minute.\nThis message will remain visible for one minute.",
                typing: false
            }, (err, res) => 
            {
                /*setTimeout(function() 
                { 
                    bot.deleteMessage({
                        channelID: info.channelID,
                        messageID: res.id
                    });
                }, 60000);*/
            });
        }
    },
    {
        cmd: "characters",
        params: "category",
        category: "main",
        execute: function(bot, info, args)
        {
            var names = [];
            for (var prop in bot.data[info.serverId].characters) {
                if (bot.data[info.serverId].characters.hasOwnProperty(prop)) {
                    names.push(prop);
                }
            }

            fs.writeFile("./characters.txt", names.join("\n"), function(err) {
                if (err) {
                    console.log(err);
                }

                bot.uploadFile({
                    to: info.channelID,
                    file: "characters.txt"
                }, (err, res) => 
                {
                    /*setTimeout(function() 
                    { 
                        bot.deleteMessage({
                            channelID: info.channelID,
                            messageID: res.id
                        });
                    }, 60000);*/
                });

                fs.unlinkSync("./characters.txt");
            });
        }
    },
    {
        cmd: "invitelink",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,//511040
                message: "https://discordapp.com/oauth2/authorize?&client_id=867764739300261889&scope=bot&permissions=339008",
                typing: false
            });
        }
    },
    {
        cmd:"update",
        params: "none",
        hidden:true,
        category: "admin",
        execute:function(bot, info, args)
        {
            var exec = require('child_process').exec;

            bot.data.update = info.channelID;

            bot.sendMessage({
                to: info.channelID,
                message: "Fetching changes...",
                typing: false
            }, function()
            {
                exec('git fetch --all', function(err, stdout, stderr) 
                {
                    exec('git log --oneline master..origin/master', function(err, stdout, stderr) 
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: (stdout == ""? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                            typing: false
                        }, function()
                        {
                            bot.suicide();
                        });
                    });
                });
            });
        }
    },
    {
        cmd: "version",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec('git log --oneline -n 1 HEAD', function(err, stdout, stderr) 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current version: ```" + stdout + "```",
                    typing:false
                })
            })
        }
    },
    {
        cmd: "dumpmem",
        params: "nothing",
        category: "Main",
        execute:function(bot, info, args)
        {
            var url = "botData.json";

            bot.sendMessage({
                to: info.channelID,
                message: "Dumping...",
                typing:false
            })

            bot.uploadFile({
                to: info.channelID,
                file: url
            }, function(error, response)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: error,
                    typing:false
                })
            })
        }
    },
    {
        cmd: "randomtest",
        params: "nothing",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            var str = rando.toString();
            var t = 0;

            for(var i = 0; i < 20; i++)
            {
                var num = wrand(1, 8, 3);
                t += num;
                str += "\n1d8 #" + i + ": " + HealthBar(num, 8);
            }
            str += "\n\naverage: " + HealthBar(t/20, 8)
            bot.sendMessage({
                to: info.channelID,
                message: str,
                typing:false
            })
        }
    }
]

function CheckIntegrity(bot, info)
{
    bot.data[info.serverId] = bot.data[info.serverId] || {characters:{}};
    bot.data[info.serverId].characters = bot.data[info.serverId].characters || {};
}

function rand(min, max)
{
    return min + (Math.floor(rando() * (max - min)));
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function CapitalizeFirstLetter(str)
{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function GetStatNum(character, statname)
{
    if(!character.SUPER)
    {
        return character[statname]
    }
    else
    {
        return character[statname] * 2;
    }
}

function RollStat(statNum)
{
    return RollMod(GetStatMod(statNum));
}

function GetStatMod(statNum)
{
    return ((statNum-10)/2);
}

function RollMod(statMod)
{
    return Math.round(wrand(1, 20, 2)) + statMod;
}

function HealthBar(current, max)
{
    var str = ""

    for(var i = 0; i < current; i++)
        str += "▰"

    for(var i = 0; i < max - current; i++)
        str += "▱";

    return str;
}

function StatBar(current, bonus, max)
{
    var str = ""

    for(var i = 0; i < current; i++)
        str += "◆"

    for(var i = 0; i < bonus; i++)
        str += "◈"

    for(var i = 0; i < max - (current + bonus); i++)
        str += "◇";

    return str;
}

function ChaosBar(current, max)
{
    var str = ""

    for(var i = 0; i < current; i++)
        str += "•"

    for(var i = 0; i < max - current; i++)
        str += "∙";

    return str;
}

function GetMaxHP(character)
{
    if(character.CLASS == "Brute")
        return Math.ceil(GetStatNum(character, "STR") + character.Emeralds);
    else
        return Math.ceil(GetStatNum(character, "CON") + character.Emeralds);
}

function GetMaxCHAOS(character)
{
    if(character.CLASS == "Magic")
        return Math.ceil((GetStatNum(character, "INT") + character.Emeralds) * 0.8);
    else
        return Math.ceil((GetStatNum(character, "CHA") + character.Emeralds) * 0.7);
}

function GetAC(character)
{
    if(character.CLASS == "Speedster")
        return Math.ceil(11 + GetStatMod(GetStatNum(character, "DEX")) + character.Emeralds);
    else
        return Math.ceil(10 + GetStatMod(GetStatNum(character, "CON")) + character.Emeralds);
}

function GetToHitMod(character)
{
    if(character.CLASS == "Techie")
        return GetStatMod(GetStatNum(character, "STR")) + GetStatMod(GetStatNum(character, "INT")) + character.Emeralds;
    else if(character.CLASS == "Speedster")
        return GetStatMod(GetStatNum(character, "DEX")) + character.Emeralds;
    else if(character.CLASS == "Magic")
        return GetStatMod(GetStatNum(character, "INT")) + character.Emeralds;
    else if(character.CLASS == "Martial")
        return 0;
    else
        return GetStatMod(GetStatNum(character, "STR")) + character.Emeralds;
}

function DamageRoll(character)
{
    var max = 6
    
    if(character.RANK == "B")
        max = 7;
    if(character.RANK == "A")
        max = 8;

    if(character.CLASS == "Martial")
        return Math.ceil(Math.max(1, wrand(1, max, 3) + Math.max(0, GetStatMod(GetStatNum(character, "DEX")) + GetStatMod(GetStatNum(character, "STR")) + character.Emeralds)));
    else if(character.CLASS == "Magic")
        return Math.ceil(Math.max(1, wrand(1, max, 3) + GetStatMod(GetStatNum(character, "CHA")) + character.Emeralds));
    else
        return Math.ceil(rand(1, max));
}

function wrand(min, max, weight)
{
    var r = rando();
    var rr = 1 - Math.pow(r, weight)
    return min + (Math.floor(rr * (max - min)));
}

function CheckStat(character, stat)
{
    switch(stat)
    {
        case "DEX": return CheckDEX(character);
        case "CON": return CheckCON(character);
        case "STR": return CheckSTR(character);
        case "INT": return CheckINT(character);
        case "CHA": return CheckCHA(character);
        case "RANK": return DamageRoll(character);
    }
}

function CheckDEX(character)
{
    return RollMod(GetStatMod(GetStatNum(character, "DEX")) + character.Emeralds);
}

function CheckCON(character)
{
    return RollMod(GetStatMod(GetStatNum(character, "CON")) + character.Emeralds)
}

function CheckSTR(character)
{
    return RollMod(GetStatMod(GetStatNum(character, "STR")) + character.Emeralds)
}

function CheckINT(character)
{
    return RollMod(GetStatMod(GetStatNum(character, "INT")) + character.Emeralds);
}

function CheckCHA(character)
{
    var result = RollMod(GetStatMod(GetStatNum(character, "CHA")) + character.Emeralds);

    if(character.CLASS == "Charismaniac")
    {
        result = Math.round(rand(1, 20));

        if(result != 1)
        {
           result = Math.max(10, result) + GetStatMod(GetStatNum(character, "CHA")) + character.Emeralds
        }
    }

    return Math.round(result);
}

function GetCharacterDetailForm(character)
{
    var total = character.DEX+ character.CON+ character.STR+ character.INT+ character.CHA;

    var emeralds = "";
    character.Emeralds = character.Emeralds || 0;

    for(var i = 0; i < character.Emeralds; i++)
        emeralds += "💎";

    return "\nNAME: " + character.Name
    +"\n" + character.RANK + " Rank " + character.CLASS
    +"\nEmerald Bonus: " + emeralds
    + (character.SUPER? "\n►► **SUPER MODE** ◄◄" : "")
    +"\n` AC`: " + GetAC(character)
    +"\n` HP`: " + HealthBar(character.HP, GetMaxHP(character))
    +"\n`DEX`: " + StatBar(GetStatNum(character, "DEX"), character.Emeralds * 2, 20)
    +"\n`CON`: " + StatBar(GetStatNum(character, "CON"), character.Emeralds * 2, 20)
    +"\n`STR`: " + StatBar(GetStatNum(character, "STR"), character.Emeralds * 2, 20)
    +"\n`INT`: " + StatBar(GetStatNum(character, "INT"), character.Emeralds * 2, 20)
    +"\n`CHA`: " + StatBar(GetStatNum(character, "CHA"), character.Emeralds * 2, 20)
    +"\n`CHAOS`: " + ChaosBar(character.CHAOS, GetMaxCHAOS(character))
    +"\n" + total + " points assigned in total.";
}